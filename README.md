# Webarchitects acme.sh / Let's Encrypt Ansible role

[![pipeline status](https://git.coop/webarch/acmesh/badges/master/pipeline.svg)](https://git.coop/webarch/acmesh/-/commits/master)

An Ansible role to install [acme.sh](https://github.com/acmesh-official/acme.sh) and to provision [Let's Encrypt](https://letsencrypt.org/) certificates on Debian.

## Usage

When `acmesh` is `true` this role will install `acme.sh` and provision a cert using the `inventory_hostname` as the certificate `commonName`.

Making the `{{ acmesh_document_root }}/.well-known/acme-challenge` directory available via HTTP for the `commonName` and all `subjectAltNames` is a requirement left to other roles (the Webarchitects [Apache](https://git.coop/webarch/apache) and [Nginx](https://git.coop/webarch/nginx) roles do this).

If more than one cert per server is required this role can be included using the `letsencrypt.yml` tasks, for example:

```yaml
- name: Provision a Let's Encrypt cert
  ansible.builtin.include_role:
    name: acmesh
    tasks_from: letsencrypt.yml
  vars:
    acmesh: true
    acmesh_common_name: example.com
    acmesh_subject_alt_names:
      - www.example.com
      - example.org
      - www.example.org
```

## Defaults

See the [defaults/main.yml](defaults/main.yml) file for the default variables and [meta/argument_specs.yml](meta/argument_specs.yml) for the variable specification.

### acmesh

The `acmesh` variable defaults to `false`, set it to `true` for the tasks in this role to be run.

### acmesh_ca

The [certificate authority](https://github.com/acmesh-official/acme.sh/wiki/Server) to use, this role currently only supports `letsencrypt`.

### acmesh_ca_file

The path to copy the intermediate cert file to after issue/renew, `acmesh_ca_file` defaults to `{{ acmesh_cert_home }}/{{ acmesh_common_name }}.ca.pem`.

### acmesh_ca_file_group

The group for the `acmesh_ca_file` file, `acmesh_ca_file_group` defaults to `ssl-cert`.

### acmesh_ca_file_mode

The mode for the `acmesh_ca_file` file, `acmesh_ca_file_mode` defaults to `0640`.

### acmesh_ca_file_owner

The owner for the `acmesh_ca_file` file, `acmesh_cert_home_owner` defaults to `root`.

### acmesh_cert_home

The directory for the certs and keys to be copied to, the `acmesh_cert_home` variable defaults to `/etc/ssl/le`.

### acmesh_cert_home_group

The group for the `acmesh_cert_home` directory, `acmesh_cert_home_group` defaults to `ssl-cert`.

### acmesh_cert_home_mode

The mode for the `acmesh_cert_home` directory, `acmesh_cert_home_mode` defaults to `0750`.

### acmesh_cert_home_owner

The owner for the `acmesh_cert_home` directory, `acmesh_cert_home_owner` defaults to `root`.

### acmesh_cert_file

The path to copy the cert file to after issue/renew, `acmesh_cert_file` defaults to `{{ acmesh_cert_home }}/{{ acmesh_common_name }}.cert.pem`.

### acmesh_cert_file_group

The group for the `acmesh_cert_file` file, `acmesh_cert_file_group` defaults to `ssl-cert`.

### acmesh_cert_file_mode

The mode for the `acmesh_cert_file` file, `acmesh_cert_file_mode` defaults to `0640`.

### acmesh_cert_file_owner

The owner for the `acmesh_cert_file` file, `acmesh_cert_home_owner` defaults to `root`.

### acmesh_common_name

The certificate `commonName`, `acmesh_common_name` defaults to `{{ inventory_hostname }}`.

### acmesh_document_root

The Apache `DocumentRoot` or Nginx `root` for the
`~/.well-known/acme-challenge` directory, `acmesh_document_root` defaults to `/var/www/html`.

### acmesh_ecc

Provision an ECC certificate, when `true`, `acmesh_ecc` defaults to `true`.

### acmesh_ecc_keylength

The ECC cert key length, `acmesh_ecc_keylength` can be `256`, the default, or `384` for Let's Encrypt as it doesn't support `512` currently.

### acmesh_fullchain_file

The path to copy the fullchain cert file to after issue/renew, `acmesh_fullchain_file` defaults to `{{ acmesh_cert_home }}/{{ acmesh_common_name }}.fullchain.pem`.

### acmesh_fullchain_file_group

The group for the `acmesh_fullchain_file` file, `acmesh_fullchain_file_group` defaults to `ssl-cert`.

### acmesh_fullchain_file_mode

The mode for the `acmesh_fullchain_file` file, `acmesh_fullchain_file_mode` defaults to `0640`.

### acmesh_fullchain_file_owner

The owner for the `acmesh_fullchain_file` file, `acmesh_fullchain_home_owner` defaults to `root`.

### acmesh_key_file

The path to copy the key file to after issue/renew, `acmesh_key_file` defaults to `{{ acmesh_cert_home }}/{{ acmesh_common_name }}.key.pem`.

### acmesh_key_file_group

The key file group, `acmesh_key_file_group` defaults to `ssl-cert`.

### acmesh_key_file_mode

The key file mode, `acmesh_key_file_mode` defaults to `0640`.

### acmesh_key_file_owner

The key file owner, `acmesh_key_file_owner` defaults to `root`.

### acmesh_reloadcmd

The command to run to restart services after provisioning a new certificate, this does not need to be set if `apache2` or `nginx` are installed as these web servers are automatically detected, by default this variable is not set.

### acmesh_state

The state for Acme.sh, can be one of:

```yaml
- absent
- installed
- present
- uninstalled
```

The `acmesh_state` defaults to `installed`, `uninstalled` requires `present`, cert provisioning requires `installed`.

### acmesh_subject_alt_names

An optional array of `subjectAltNames` for the certificate.

## Legacy variables

This role supports the use of [legacy variables](tasks/legacy.yml), note that the old `subject_alt_names` array expected the `common_name` to be included in it, the replacement `acmesh_subject_alt_names` variable doesn't so the `common_name` is removed when setting `acmesh_subject_alt_names` if it is present in a `subject_alt_names` array.

## Copyright

Copyright 2018-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
